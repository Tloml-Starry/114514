import fetch from "node-fetch";

const length = 200 // 长度，10-10000
const regex = /^(#|\/)?(狗屁|gp)(.*)$/
export class example extends plugin {
    constructor() {
        super({
            name: '狗屁不通文章生成',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: regex,
                    fnc: 'nonsenseArticleGeneration'
                }
            ]
        });
    }

    async nonsenseArticleGeneration(e) {
        const match = e.msg.match(regex)
        const title = match[3]
        const Fetch = await fetch(`https://oiapi.net/API/Bullshit/?title=${title}&length=${length}`);
        const Json = await Fetch.json();
        if (Json['code'] !== 1) { return e.reply(Json['message']) }
        return e.reply(Json['message'])
    }
}
