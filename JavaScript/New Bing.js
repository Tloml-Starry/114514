import fetch from "node-fetch"

const regex = /^(#|\/)?NewBing(.*)$/
export class example extends plugin {
    constructor() {
        super({
            name: 'New Bing',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: regex,
                    fnc: 'NewBing'
                }
            ]
        })
    }
    async NewBing(e) {
        const msg = e.msg.match(regex)
        const Fetch = await fetch(`http://ovoa.cc/api/Bing.php?msg=${msg}?&model=down&type=json`)
        const Json = await Fetch.json();
        const Code = Json['code']
        if (Code === 200) {
            return e.reply([segment.at(e.user_id), `\n回复：${Json['content']}`])
        } else {
            return e.reply(['请求错误，状态码：' + Code])
        }
    }
}