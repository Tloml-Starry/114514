export class example extends plugin {
    constructor() {
        super({
            name: '抖音AI图生图',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?抖音(A|a)(I|i)生图$/,
                    fnc: 'DouYin_AI_Pictrue'
                }
            ]
        })
    }
    async DouYin_AI_Pictrue(e) {
        if (!e.img) {
            e.reply('请发送图片')
            this.setContext('AI_Pictrue')
        } else {
            return e.reply([segment.image(`https://oiapi.net/API/AiImage/?url=${e.img[0]}`)])
        }
    }
    AI_Pictrue(e) {
        if (!this.e.img) {
            e.reply('请发送图片')
        } else {
            e.reply([segment.image(`https://oiapi.net/API/AiImage/?url=${this.e.img[0]}`)])
            return this.finish('AI_Pictrue')
        }
    }
}