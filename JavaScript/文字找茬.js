import fs from 'fs';
import fetch from "node-fetch";

const FILE_PATH = 'data/TextLookForProblems'
const REGEX = /^(#|\/)?列(.*)横(.*)$/
fs.mkdirSync(FILE_PATH, { recursive: true });
export class example extends plugin {
    constructor() {
        super({
            name: '文字找茬',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?文字找茬(简单|普通|困难)?$/,
                    fnc: 'start'
                },
                {
                    reg: /^(#|\/)?结束文字找茬$/,
                    fnc: 'end'
                },
                {
                    reg: REGEX,
                    fnc: 'Answer'
                }
            ]
        });
    }

    async start(e) {
        const USER_FILE = `${FILE_PATH}/${e.user_id}.json`

        if (!fs.existsSync(USER_FILE)) {
            const GAME_DATA = { State: true }
            fs.writeFileSync(USER_FILE, JSON.stringify(GAME_DATA, null, 4), 'utf8')
        }

        const READ_FILE = fs.readFileSync(USER_FILE, 'utf8')
        const USER_DATA = JSON.parse(READ_FILE)

        if (!USER_DATA['State']) {
            logger.mark(USER_DATA['State'])
            return e.reply([
                segment.at(e.user_id),
                '\n你的上一局游戏还没结束！\n',
                USER_DATA['message']['result'],
                '\n找不到请发送#结束文字找茬'
            ])
        }

        let X = 8; let Y = 8;
        if (/^(#|\/)?文字找茬普通$/.test(e.msg)) {
            X += 8; Y += 8
        } else if (/^(#|\/)?文字找茬困难$/.test(e.msg)) {
            X += 16; Y += 16
        }

        const FETCH_DATA = await fetch(`https://oiapi.net/API/ChineseFindDifferent/?x=${X}&y=${Y}`);
        const DATA_JSON = await FETCH_DATA.json();

        if (DATA_JSON['code'] !== 1) { return e.reply('数据异常') }

        const GAME_DATA = {
            State: false,
            message: {
                result: DATA_JSON['data']['result'],
                x: DATA_JSON['data']['x'],
                y: DATA_JSON['data']['y']
            }
        }
        fs.writeFileSync(USER_FILE, JSON.stringify(GAME_DATA, null, 4), 'utf8')

        return e.reply([
            segment.at(e.user_id),
            '\n使用指令#列1横2发送答案\n',
            DATA_JSON['message'],
            '\n找不到请发送#结束文字找茬'
        ])
    }

    async end(e) {
        const USER_FILE = `${FILE_PATH}/${e.user_id}.json`

        if (!fs.existsSync(USER_FILE)) {
            return e.reply([
                segment.at(e.user_id),
                '\n您没有发起过文字找茬\n游玩请发送#文字找茬'
            ])
        }

        const READ_FILE = fs.readFileSync(USER_FILE, 'utf8')
        const USER_DATA = JSON.parse(READ_FILE)

        if (USER_DATA['State']) {
            return e.reply([
                segment.at(e.user_id),
                '\n您的游戏已经结束了\n游玩下一把请发送#文字找茬'
            ])
        } else {
            USER_DATA['State'] = true
            fs.writeFileSync(USER_FILE, JSON.stringify(USER_DATA, null, 4), 'utf8')

            return e.reply([
                segment.at(e.user_id),
                '已为您结束游戏',
                '\n答案是：列' + USER_DATA['message']['y'] + ' 横' + USER_DATA['message']['x']
            ])
        }
    }

    async Answer(e) {
        const USER_FILE = `${FILE_PATH}/${e.user_id}.json`

        if (!fs.existsSync(USER_FILE)) {
            return e.reply([
                segment.at(e.user_id),
                '您没有发起过文字找茬\n游玩请发送#文字找茬'
            ])
        }

        const READ_FILE = fs.readFileSync(USER_FILE, 'utf8')
        const USER_DATA = JSON.parse(READ_FILE)

        if (USER_DATA['State']) {
            return e.reply([
                segment.at(e.user_id),
                '\n你的游戏已经结束！请发送#文字找茬开启下一把'
            ])
        }

        const MATCH = e.msg.match(REGEX)
        const X = parseFloat(MATCH[2])
        const Y = parseFloat(MATCH[3])

        if (USER_DATA['message']['x'] === Y || USER_DATA['message']['y'] === X) {
            USER_DATA['State'] = true
            fs.writeFileSync(USER_FILE, JSON.stringify(USER_DATA, null, 4), 'utf8')
            return e.reply([
                segment.at(e.user_id),
                '\n答对啦！\n发送#文字找茬开启下一把吧！'
            ])
        } else {
            return e.reply([
                segment.at(e.user_id),
                '\n答错啦~\n找不到请发送#结束文字找茬'
            ])
        }
    }
}