const regex = /^(#|\/)?BA(.*):(.*)$/
export class example extends plugin {
    constructor() {
        super({
            name: '碧蓝档案高仿文字图',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: regex,
                    fnc: 'BlueArchive'
                }
            ]
        })
    }
    async BlueArchive(e) {
        const Text = e.msg.match(regex)
        const startText = Text[2]
        const endText = Text[3]
        if (startText.length > 64 || endText.length > 64) { return e.reply('输入超出限制！前后仅支持64个字符') }
        return e.reply([segment.image(`https://oiapi.net/API/BlueArchive/?startText=${startText}&endText=${endText}`)])
    }
}