import fetch from "node-fetch";

const count = 10 // 返回排行榜数量，推荐不要太高，可能会导致无法发送
export class example extends plugin {
    constructor() {
        super({
            name: '手机跑分排行榜',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?手机跑分排行$/,
                    fnc: 'MPR'
                }
            ]
        });
    }

    async MPR(e) {
        const FETCH_DATA = await fetch(`https://api.yujn.cn/api/paofen.php?count=${count}&msg=Android`);
        const DATA_JSON = await FETCH_DATA.json();
        const MESSAGE = ['手机跑分排行榜']
        for (let i = 0; i < count; i++) {
            const DATA = DATA_JSON['data'][i]
            const MSG = [
                '\n排名：' + DATA['Index'],
                '\n手机名称：', DATA['Name'],
                '\n手机配置：', DATA['Deploy'],
                '\n跑分总数：' + DATA['Grade']
            ]
            MESSAGE.push(MSG)
        }
        return e.reply(MESSAGE)
    }
}
