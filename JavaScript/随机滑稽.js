import fetch from 'node-fetch'
import _ from 'lodash'

const URL = 'https://gitee.com/Tloml-Starry/resources/raw/master/resources/img/Hua%20Ji/'
const GX = 'https://gitee.com/Tloml-Starry/resources/blob/master/UPDATE.md'
export class example extends plugin {
    constructor() {
        super({
            name: '[娱乐]随机滑稽',
            event: 'message',
            priority: 1,
            rule: [{
                reg: /^(#|\/)?(随机|来点)?滑稽$/,
                fnc: 'HJ'
            }, {
                reg: /^(#|\/)?滑稽库$/,
                fnc: 'HJK'
            }]
        })
    }

    async HJ(e) {
        const HJ_DATA = await (await fetch(`${URL}/H%20J.json`)).json()
        const HJ_IMG = `${URL}${_.random(0, HJ_DATA['n'])}.jpg`

        return e.reply((e.adapter === 'QQBot') ? [
            segment.image(HJ_IMG),
            Bot.Button([[
                { label: '随机滑稽', enter: true },
                { label: '滑稽库', enter: true }
            ], [
                { label: '为滑稽库添加滑稽', link: GX }
            ]])
        ] : [
            segment.image(HJ_IMG)
        ])
    }

    async JHK(e) {
        const HJ_DATA = await (await fetch(`${URL}H%20J.json`)).json()
        return e.reply((e.adapter === 'QQBot') ? [
            `> 云滑稽库当前滑稽图共: ${HJ_DATA['n']}张`,
            Bot.Button([[
                { label: '随机滑稽', enter: true }
            ], [
                { label: '为滑稽库添加滑稽', link: GX }
            ]])
        ] : [
            `云滑稽库当前滑稽图共: ${HJ_DATA['n']}张`,
            '\n贡献滑稽图：\n', GX
        ])
    }
}