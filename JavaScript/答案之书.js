import fetch from "node-fetch";

export class example extends plugin {
    constructor() {
        super({
            name: '答案之书',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?答案之书$/,
                    fnc: 'AnswersBook'
                }
            ]
        });
    }

    async AnswersBook(e) {
        const FETCH_DATA = await fetch('https://oiapi.net/API/BOfA');
        const DATA_JSON = await FETCH_DATA.json();
        return e.reply([segment.at(e.user_id), '\n', DATA_JSON['message']])
    }
}
