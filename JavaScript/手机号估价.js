import fetch from "node-fetch"

export class example extends plugin {
    constructor() {
        super({
            name: '手机号估价',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?手机号(估价|估值)[0-9]{11}$/,
                    fnc: 'Phone_Valuation'
                }
            ]
        })
    }
    async Phone_Valuation(e) {
        const Phone = e.msg.replace(/#|\/|手机号|估值|估价/g, '')
        const Fetch = await fetch(`https://api.ovooa.cc/api/Number_valuation?phone=${Phone}`)
        const Json = await Fetch.json();
        const Code = Json['code']
        if (Code === "400") {
            return e.reply('手机号不正确')
        } else if (Code === "200") {
            return e.reply([segment.at(e.user_id), `\n估价：${Json['money']} $`])
        }
    }
}