import fetch from "node-fetch";

const count = 10 // 返回排行榜数量，推荐不要太高，可能会导致无法发送
export class example extends plugin {
    constructor() {
        super({
            name: 'Steam在线游戏人数排行',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?Steam在线游戏排行$/,
                    fnc: 'steamOnlineGameNumberRanking'
                }
            ]
        });
    }

    async steamOnlineGameNumberRanking(e) {
        const FETCH_DATA = await fetch(`http://api.yujn.cn/api/steam.php?type=json&count=${count}`);
        const DATA_JSON = await FETCH_DATA.json();
        const MESSAGE = ['Steam在线游戏人数排行']
        for (let i = 0; i < count; i++) {
            const DATA = DATA_JSON['data'][i]
            const MSG = [
                '\n排名：' + DATA['index'],
                '\n中文名：', DATA['name_cn'],
                '\n英文名：', DATA['name'],
                '\n在线人数：' + DATA['current'],
                '\n今日峰值：' + DATA['day_peak'],
                segment.image(DATA['avatar']), '\n'
            ]
            MESSAGE.push(MSG)
        }
        return e.reply(MESSAGE)
    }
}
