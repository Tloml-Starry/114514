import fetch from "node-fetch";

const count = 10 // 返回排行榜数量，推荐不要太高，可能会导致无法发送
export class example extends plugin {
    constructor() {
        super({
            name: '手机处理器排行榜',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?手机处理器排行$/,
                    fnc: 'MPPR'
                }
            ]
        });
    }

    async MPPR(e) {
        const FETCH_DATA = await fetch(`https://api.yujn.cn/api/cpu.php?count=${count}`);
        const DATA_JSON = await FETCH_DATA.json();
        const MESSAGE = ['手机处理器排行榜']
        for (let i = 0; i < count; i++) {
            const DATA = DATA_JSON['data'][i]
            const MSG = [
                '\n排名：' + DATA['index'],
                '\n处理器名称：', DATA['Name'],
                '\n跑分总数：' + DATA['Grade']
            ]
            MESSAGE.push(MSG)
        }
        return e.reply(MESSAGE)
    }
}
