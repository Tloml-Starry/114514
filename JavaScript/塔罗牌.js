import fetch from "node-fetch";
import common from "../../lib/common/common.js";

export class example extends plugin {
    constructor() {
        super({
            name: '塔罗牌',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?塔罗牌$/,
                    fnc: 'tarotCards'
                }
            ]
        });
    }

    async tarotCards(e) {
        const Fetch = await fetch('https://oiapi.net/API/Tarot');
        const Json = await Fetch.json();
        const Data = Json['data'];
        const msgArr = [];
        const zOrN = [];
        const zOrNName = [];

        for (let i = 0; i < 4; i++) {
            let position = i + 1;
            if (position === 4) {
                position = '切牌';
            } else {
                position = `第${i + 1}张牌`;
            }

            if (!Data[i]['正位']) {
                zOrN.push(Data[i]['逆位']);
                zOrNName.push('逆位');
            } else {
                zOrN.push(Data[i]['正位']);
                zOrNName.push('正位');
            }

            msgArr.push(
                `${position}${Data[i]['meaning']}` +
                `\nName：[${zOrNName[i]}] ${Data[i]['name_cn']}(${Data[i]['name_en']})` +
                `\n${zOrN[i]}`,
                segment.image(Data[i]['pic'])
            );
        }
        return e.reply(await common.makeForwardMsg(e, msgArr));
    }
}
