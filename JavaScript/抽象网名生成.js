import fetch from "node-fetch";

const REGEX = /^(#|\/)?网名生成(.*)$/
const num = 10 // 最高支持生成50个
export class example extends plugin {
    constructor() {
        super({
            name: '抽象网名生成',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: REGEX,
                    fnc: 'Abstraction'
                }
            ]
        });
    }

    async Abstraction(e) {
        const MATCH = e.msg.match(REGEX)
        const NAME = MATCH[2]
        const FETCH_DATA = await fetch(`http://api.yujn.cn/api/tsfh.php?type=json&msg=${NAME}&num=${num}`);
        const DATA_JSON = await FETCH_DATA.json();
        const MESSAGE = [segment.at(e.user_id)]
        for (let i = 0; i < num; i++) {
            MESSAGE.push(`\n${DATA_JSON['data'][i]}`)
        }
        return e.reply(MESSAGE)
    }
}
