import fetch from "node-fetch";

const count = 20 // 返回排行榜数量，推荐不要太高，可能会导致无法发送
export class example extends plugin {
    constructor() {
        super({
            name: '福布斯全球富豪榜',
            dsc: 'example',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?福布斯富豪榜$/,
                    fnc: 'RichList'
                }
            ]
        });
    }

    async RichList(e) {
        const FETCH_DATA = await fetch(`https://api.yujn.cn/api/fbs.php?type=json&count=${count}`);
        const DATA_JSON = await FETCH_DATA.json();
        const MESSAGE = ['福布斯全球富豪榜']
        for (let i = 0; i < count; i++) {
            const name = DATA_JSON[i]['name']
            const img = DATA_JSON[i]['img']
            const money = DATA_JSON[i]['money']
            const brand = DATA_JSON[i]['brand']
            const state = DATA_JSON[i]['state']
            MESSAGE.push(
                '\n' + DATA_JSON[i]['index'] + ':', name,
                segment.image(img),
                '\n资产：', money,
                '\n来源：', brand,
                '\n国籍：', state
            )
        }
        return e.reply(MESSAGE)
    }
}
