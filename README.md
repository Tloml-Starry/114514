<!--
2024/11/13 1 UPDATE
 -->
# 傅卿何 example
<!-- 2024/03/03开始记录的访问量 -->
![动态访问量](https://count.kjchmc.cn/get/@Plugin-Example?theme=rule34)  
本仓库提供[Yunzai-Bot V3](https://gitee.com/Le-niao/Yunzai-Bot) & [A-Yunzai](https://gitee.com/ningmengchongshui/azai-bot)小型插件  
本库持续更新，主要以对接API提供功能为主，有好玩的API可以发起[issues](https://gitee.com/Tloml-Starry/Plugin-Example/issues)  
如出现插件报错或功能不适配等情况请及时联系[作者](https://gitee.com/Tloml-Starry)或发起[issues](https://gitee.com/Tloml-Starry/Plugin-Example/issues)  
如果你对本仓库表示支持，请点点你手中的Star
## 联系我
点开我的主页私信我  
QQ: [ 3620060826, 1947425850 ] `大号经常冻结，急可加小号`  
QQ群: 392665563 `可能申请进不了，加QQ让我拉你进去`

---

## 以下为插件列表，点击展开查看详情
<details>
<summary>点击展开 · 留言板</summary>

#### 介绍
留言功能，多群共用同一个板，可以在上面发布逆天言论，主人可以删除指定留言
#### 安装
```
curl -o "./plugins/example/messageBoard.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/messageBoard.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 查看留言板 | 第一个留言既是使用说明 |
</details>

<details>
<summary>点击展开 · 原神BOSS击杀数量查询</summary>

#### 介绍
如题，字面意思，查询世界BOSS被你kill了几次
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-B4J63)
直链下载请将"原神世界BOSS.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/原神世界BOSS.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E5%8E%9F%E7%A5%9E%E4%B8%96%E7%95%8CBOSS.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| #Boss击杀 | 查询世界boss击杀次数（需要Ck） |
| #Boss击杀排行 | 字面意思 |
</details>

<details>
<summary>点击展开 · 原神背包查询</summary>

#### 介绍
如题，字面意思，但不是全部物品
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-zAJ63)
直链下载请将"原神背包查询.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/原神背包查询.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E5%8E%9F%E7%A5%9E%E8%83%8C%E5%8C%85%E6%9F%A5%E8%AF%A2.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| #原神背包 | 查询原神背包（需要Ck） |
</details>

<details>
<summary>点击展开 · CS2库存查询</summary>

#### 介绍
使用机器人查询CS2库存并返回
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-DdJ63)
直链下载请将"CS2库存查询.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/CS2库存查询.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/CS2%E5%BA%93%E5%AD%98%E6%9F%A5%E8%AF%A2.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| CS查询库存 | 查询CS2库存 |
| 绑定steamid | 绑定SteamID |
</details>

<details>

<summary>点击展开 · 光遇国服身高查询</summary>


#### 介绍
使用机器人查询光遇角色身高数据等
#### 安装
```
curl -o "./plugins/example/[Sky]身高查询[用户自填Key].js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/[Sky]%E8%BA%AB%E9%AB%98%E6%9F%A5%E8%AF%A2[%E7%94%A8%E6%88%B7%E8%87%AA%E5%A1%ABKey].js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 身高查询 | 查询光遇角色身高数据等 |
| 绑定sid,fc,key | 绑定查询前置条件 |
| 设置公共Key | 设置查询使用的公共key |
</details>

<details>
<summary>点击展开 · 自动续火,打卡,抽字符</summary>

#### 介绍
让机器人自动在群内发送消息群打卡和抽字符
#### 安装
```
curl -o "./plugins/example/[Task]群打卡&诗词续火&抽字符 V1.1.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/[Task]%E7%BE%A4%E6%89%93%E5%8D%A1&%E8%AF%97%E8%AF%8D%E7%BB%AD%E7%81%AB&%E6%8A%BD%E5%AD%97%E7%AC%A6%20V1.1.js"
```
#### 使用命令
| 指令 | 作用 |
| ---------- | ----------- |
| 执行续火 | 手动执行续火任务 |
#### 配置
请打开JS进行配置  
文件内有详细介绍
</details>

<details>
<summary>点击展开 · 鸣潮卡池多久未复刻查询</summary>

#### 介绍
提供鸣潮卡池多久未复刻查询
#### 安装
```
curl -o "./plugins/example/鸣潮卡池信息查询.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E9%B8%A3%E6%BD%AE%E5%8D%A1%E6%B1%A0%E4%BF%A1%E6%81%AF%E6%9F%A5%E8%AF%A2V1.0.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 鸣潮卡池多久未复刻 | 角色卡池多久未复刻 |
</details>

<details>
<summary>点击展开 · 卡池信息查询</summary>

#### 介绍
原神，星穹铁道和绝区零的卡池信息查询   
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-6TB63.html)
直链下载请将"卡池信息查询.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/卡池信息查询.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E5%8D%A1%E6%B1%A0%E4%BF%A1%E6%81%AF%E6%9F%A5%E8%AF%A2.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 原神卡池多久未复刻 | 查看原神角色卡池多久未复刻 |
| 星铁卡池多久未复刻 | 查看星铁角色卡池多久未复刻 |
| 绝区零卡池多久未复刻 | 查看绝区零角色卡池多久未复刻 |
| #温迪卡池记录 | 查看原神温迪复刻次数和复刻时间以及版本，可替换为其他角色 |
| *希儿卡池记录 | 查看星铁希儿复刻次数和复刻时间以及版本，可替换为其他角色 |
| %艾莲卡池记录 | 查看绝区零艾莲复刻次数和复刻时间以及版本，可替换为其他角色 |
### 注：卡池记录指令必须附带前缀，原神用#|星铁用*|绝区零用%
</details>

<details>
<summary>点击展开 · 电子木鱼</summary>

#### 介绍
敲电子木鱼，赛博成圣
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-ZNa63.html)
直链下载请将"电子木鱼.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/电子木鱼.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E7%94%B5%E5%AD%90%E6%9C%A8%E9%B1%BC.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 敲木鱼 | 敲一次木鱼 |
| (今日\本群\本群今日)功德榜 | 顾名思义，功德榜，不附带(今日\本群\本群今日)可查看总排行榜（是不是搞不懂为什么一个敲木鱼还要有排行榜 |
| 木鱼记录 | 查看自己敲木鱼记录 |
</details>

<details>
<summary>点击展开 · 刷听歌时长</summary>

##### 介绍
刷取QQ音乐听歌时长，同时也可以刷取音响力
##### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-aTf63.html)  
直链下载请将"刷听歌时长.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/刷听歌时长.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E5%88%B7%E5%90%AC%E6%AD%8C%E6%97%B6%E9%95%BF.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 查询听歌时长 | 查询QQ音乐听歌排行榜 |
| #Q音登录 | 执行登录QQ音乐操作 |
| over | 执行完上述指令之后获取key等数据 |
| 刷听歌时长 | 刷取听歌时长和音响力 |
#### 配置
打开文件配置  
num 为刷取遍数，最高412  
mid 为指定音乐ID[如何获取音乐ID?](https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/PICTRUE/QQ音乐ID获取.png)
</details>

<details>
<summary>点击展开 · 抖音AI图生图</summary>

#### 介绍
借助抖音的免费接口以图生图，不可以有涩涩内容，返回换个图的可能是识别到涩涩了
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-Kjp63.html)  
直链下载请将"抖音AI图生图.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/抖音AI图生图.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E6%8A%96%E9%9F%B3AI%E5%9B%BE%E7%94%9F%E5%9B%BE.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 抖音AI生图(图片) | 以图生图 |
</details>

<details>
<summary>点击展开 · 碧蓝档案高仿文字图</summary>

#### 介绍
碧蓝档案图标文字图片的高仿图
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-yjp63.html)  
直链下载请将"碧蓝档案高仿文字图.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/碧蓝档案高仿文字图.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E7%A2%A7%E8%93%9D%E6%A1%A3%E6%A1%88%E9%AB%98%E4%BB%BF%E6%96%87%E5%AD%97%E5%9B%BE.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| BA(开头):(结尾) | 制作一张高仿碧蓝档案文字图 |
</details>

<details>
<summary>点击展开 · 手机号估价</summary>

#### 介绍
对手机号进行估算价格
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-pTf63.html)  
直链下载请将"手机号估价.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/手机号估价.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E6%89%8B%E6%9C%BA%E5%8F%B7%E4%BC%B0%E4%BB%B7.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 手机号估价(手机号) | 对手机号进行估算价格 |
</details>

<details>
<summary>点击展开 · 塔罗牌</summary>

#### 介绍
随机塔罗牌
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-Cvf63.html)  
直链下载请将"塔罗牌.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/塔罗牌.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E5%A1%94%E7%BD%97%E7%89%8C.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 塔罗牌 | 输出一张塔罗牌 |
</details>

<details>
<summary>点击展开 · New Bing</summary>

#### 介绍
New Bing为微软必应中基于ChatGPT Ai人工智能,本身就为GPT-4
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-fTf63.html)  
直链下载请将"New Bing.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/New Bing.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/New%20Bing.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| NewBing(问题) | 对必应进行提问 |
</details>

<details>
<summary>点击展开 · 狗屁不通</summary>

#### 介绍
狗屁不通文章生成器
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-Wvf63.html)  
直链下载请将"狗屁不通.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/狗屁不通.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E7%8B%97%E5%B1%81%E4%B8%8D%E9%80%9A.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 狗屁(主题) | 根据主题生成牛头不对马嘴的内容 |
| gp(主题) | 同上 |
#### 配置
打开文件配置  
length 为文章长度，支持10-10000  
</details>

<details>
<summary>点击展开 · 文字找茬</summary>

#### 介绍
汉字找不同游戏
#### 安装 
```
curl -o "./plugins/example/文字找茬.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E6%96%87%E5%AD%97%E6%89%BE%E8%8C%AC.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 文字找茬 | 发起游戏，可选难度（简单|普通|困难），不选默认简单 |
| 结束文字找茬 | 找不到结束游戏查看答案 |
| 列1横2 | 发送答案结束本局文字找茬 | 
</details>

<details>
<summary>点击展开 · 答案之书</summary>

#### 介绍
带着心中疑问寻找答案
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-9df63.html)  
直链下载请将"答案之书.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/答案之书.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E7%AD%94%E6%A1%88%E4%B9%8B%E4%B9%A6.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 答案之书 | 随机一种答案，请带着心中疑问使用 |
</details>

<details>
<summary>点击展开 · 表情包搜索</summary>

#### 介绍
表情包搜索，QQ输入框同款
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-Wdf63.html)  
直链下载请将"表情包搜索.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/表情包搜索.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E8%A1%A8%E6%83%85%E5%8C%85%E6%90%9C%E7%B4%A2.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 表情包搜索(关键字) | 搜索表情包并返回 |
</details>

<details>
<summary>点击展开 · 抽象网名生成</summary>

#### 介绍
返回特殊符号网名，部分符号编码有问题
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-dhf63.html)  
直链下载请将"抽象网名生成.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/抽象网名生成.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E6%8A%BD%E8%B1%A1%E7%BD%91%E5%90%8D%E7%94%9F%E6%88%90.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 网名生成(关键字) | 生成网名 |
#### 配置
打开文件配置  
num 为生成数量，最高支持50个  
</details>

<details>
<summary>点击展开 · Steam在线游戏人数排行</summary>

#### 介绍
返回游戏当前在线人数排行
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-Hhf63.html)  
直链下载请将"Steam在线游戏人数排行.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/Steam在线游戏人数排行.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/Steam%E5%9C%A8%E7%BA%BF%E6%B8%B8%E6%88%8F%E4%BA%BA%E6%95%B0%E6%8E%92%E8%A1%8C.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| Steam在线游戏排行 | 查询排行榜 |
#### 配置
打开文件配置  
count 返回排行榜数量，推荐不要太高，可能会导致无法发送  
</details>

<details>
<summary>点击展开 · Epic免费游戏</summary>

#### 介绍
查询当前Epic免费游戏和即将推出的免费游戏
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-gNJ63)  
直链下载请将"Epic免费游戏.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/Epic免费游戏.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/Epic%E5%85%8D%E8%B4%B9%E6%B8%B8%E6%88%8F.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 喜加一 | 查询当前Epic免费游戏和即将推出的免费游戏 | 
</details>

<details>
<summary>点击展开 · 福布斯全球富豪榜</summary>

#### 介绍
查询福布斯全球富豪排行榜
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-Mhf63.html)  
直链下载请将"福布斯全球富豪榜.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/福布斯全球富豪榜.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E7%A6%8F%E5%B8%83%E6%96%AF%E5%85%A8%E7%90%83%E5%AF%8C%E8%B1%AA%E6%A6%9C.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 福布斯富豪榜 | 查询福布斯全球富豪排行榜 | 
</details>

<details>
<summary>点击展开 · 手机处理器排行榜</summary>

#### 介绍
查询手机处理器跑分排行榜
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-whf63.html)  
直链下载请将"手机处理器排行榜.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/手机处理器排行榜.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E6%89%8B%E6%9C%BA%E5%A4%84%E7%90%86%E5%99%A8%E6%8E%92%E8%A1%8C%E6%A6%9C.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 手机处理器排行 | 查询手机处理器跑分排行榜 | 
</details>

<details>
<summary>点击展开 · 手机跑分排行榜</summary>

#### 介绍
查询手机跑分排行榜
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-ghf63.html)  
直链下载请将"手机跑分排行榜.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/手机跑分排行榜.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E6%89%8B%E6%9C%BA%E8%B7%91%E5%88%86%E6%8E%92%E8%A1%8C%E6%A6%9C.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 手机跑分排行 | 手机跑分排行榜 | 
</details>

<details>
<summary>点击展开 · 王者战绩&信息查询</summary>

#### 介绍
查询王者荣耀战绩和王者营地主页信息
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-HMf63.html)  
直链下载请将"王者战绩查询.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/王者战绩查询.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E7%8E%8B%E8%80%85%E6%88%98%E7%BB%A9%E6%9F%A5%E8%AF%A2.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 绑定营地ID | 绑定王者营地ID | 
| 战绩查询3 | 查询最近的三条战绩 |
| 我的营地主页 | 查询营地主页信息，对局次数，胜利次数等 |
</details>

<details>
<summary>点击展开 · 发病语录</summary>

#### 介绍
发病语录
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-XYf63.html)  
直链下载请将"发病语录.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/发病语录.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E5%8F%91%E7%97%85%E8%AF%AD%E5%BD%95.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 发病语录 | 发送发病语录 |
</details>

<details>
<summary>点击展开 · 随机滑稽</summary>

#### 介绍
随机滑稽表情包
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-IPf63.html)  
直链下载请将"随机滑稽.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/随机滑稽.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E9%9A%8F%E6%9C%BA%E6%BB%91%E7%A8%BD.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 随机滑稽 | 发送随机滑稽表情包 |
| 滑稽库 | 查看有多少滑稽表情包 |
</details>

<details>
<summary>点击展开 · [陈泽][丁真][孙笑川]语音合成</summary>

#### 介绍
合成[陈泽][丁真][孙笑川]语音发送
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-plf63.html)  
直链下载请将"[陈泽][丁真][孙笑川]语音合成.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/[陈泽][丁真][孙笑川].js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/[%E9%99%88%E6%B3%BD][%E4%B8%81%E7%9C%9F][%E5%AD%99%E7%AC%91%E5%B7%9D]%E8%AF%AD%E9%9F%B3%E5%90%88%E6%88%90.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 陈泽说(内容) | 合成陈泽语音 |
| 丁真说(内容) | 合成丁真语音 |
| 孙笑川说(内容) | 合成孙笑川语音 |
</details>

<details>
<summary>点击展开 · 最近节日查询</summary>

#### 介绍
查询最近的节日离现在还要几天
#### 安装
使用命令或选择直链下载 [点我下载](https://www.123pan.com/s/Q1g5Vv-bNa63.html)  
直链下载请将"最近节日查询.js"放置在"Yunzai/plugins/example"内
```
curl -o "./plugins/example/最近节日查询.js" "https://gitee.com/Tloml-Starry/Plugin-Example/raw/master/JavaScript/%E6%9C%80%E8%BF%91%E8%8A%82%E6%97%A5%E6%9F%A5%E8%AF%A2.js"
```
#### 使用指令
| 指令 | 作用 |
| ---------- | ----------- |
| 最近节日 | 查询最近的节日离现在还要几天 |
#### 配置
打开文件配置  
n 为查询节日数量
</details>

## 致谢
| Nickname | Contribution |
| ---------- | :---------: |
| [OIAPI](https://oiapi.net/) | [刷听歌时长](https://oiapi.net/?action=doc&id=19)丨[抖音AI图生图](https://oiapi.net/?action=doc&id=13)丨[姓名测算](https://oiapi.net/?action=doc&id=15)丨[碧蓝档案高仿文字图](https://oiapi.net/?action=doc&id=29)丨[塔罗牌](https://oiapi.net/?action=doc&id=10)丨[狗屁不通](https://oiapi.net/?action=doc&id=66)丨[文字找茬](https://oiapi.net/?action=doc&id=51)丨[答案之书](https://oiapi.net/?action=doc&id=27)丨[表情包搜索](https://oiapi.net/?action=doc&id=45)丨[发病语录](https://oiapi.net/?action=doc&id=2) |
| [独角兽API](https://d.ovooa.cc/) | [手机号估价](https://d.ovooa.cc/api-114370905) |
| [小虫API](http://ovoa.cc/) | [New Bing]()丨[陈泽语音合成](https://ovoa.cc/?action=doc&name=chenzetts)丨[丁真语音合成](https://ovoa.cc/?action=doc&name=dingzhentts)丨[孙笑川语音合成](https://ovoa.cc/?action=doc&name=sunxiaochuantts) |
| [遇见API](https://api.yujn.cn/) | [抽象网名生成](https://api.yujn.cn/?action=interface&id=270)丨[Steam在线游戏人数排行](https://api.yujn.cn/?action=interface&id=314)丨[福布斯全球富豪榜](https://api.yujn.cn/?action=interface&id=315)丨[手机处理器排行榜](https://api.yujn.cn/?action=interface&id=335)丨[手机跑分排行榜](https://api.yujn.cn/?action=interface&id=336)丨[王者战绩](https://api.yujn.cn/?action=interface&id=378)丨[王者营地主页](https://api.yujn.cn/?action=interface&id=377) |